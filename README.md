# yusynth_vca

## Description

Design files for a basic DC-coupled VCA based on yusynth's VCA,
created using the free and open source toolkit LibrePCB.

Intended for operation on +/- 12V or +/- 15V

## License

See the LICENSE file
